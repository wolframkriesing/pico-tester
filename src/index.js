const trimErrorStack = (stack, atStackTraceLine = 3) => {
  const lines = stack.split('\n');
  let numStackTraceLines = 0;
  return lines.filter((line) => {
    const isStackTraceLine = line.trim().startsWith('at ');
    if (isStackTraceLine) {
      numStackTraceLines++;
    }
    return numStackTraceLines <= atStackTraceLine;
  }).join('\n');
};

const rootGroup = {
  level: 0,
};

const printGroupName = (name, level) => {
  const prefix = new Array(level).fill('  ').join('');
  console.log(`${prefix}${name}`);
};

const printPassedTest = (name, level) => {
  const prefix = new Array(level + 1).fill('  ').join('');
  console.log(`${prefix}✓ ${name}`);
};

const printFailedTest = (name, level) => {
  const prefix = new Array(level + 1).fill('  ').join('');
  console.log(`${prefix}❌ ${name}`);
};

export const newGroup = (groupName, parentGroup = rootGroup) => {
  let printedGroupName = false;
  const test = (testName, fn) => {
    if (printedGroupName === false) {
      printGroupName(groupName, parentGroup.level);
      printedGroupName = true;
    }

    try {
      fn();
    } catch (e) {
      printFailedTest(testName, parentGroup.level)
      console.log(trimErrorStack(e.stack));
      process.exit(1);
    }
    printPassedTest(testName, parentGroup.level);
  };
  return {
    test,
    level: parentGroup.level + 1,
  };
};
