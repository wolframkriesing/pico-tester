import {newGroup} from '../src/index.js';
import {strict as assert} from 'assert';

let group;
const it = (name, fn) => {
  group.test(name, fn);
};
const describe = (groupName, fn) => {
  group = newGroup(groupName);
  fn();
};

describe('GIVEN a test group', () => {
  it('with one passing test', () => {
    assert(true);
  });
});
