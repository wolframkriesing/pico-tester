import {newGroup} from '../src/index.js';
import {strict as assert} from 'assert';

const group = newGroup('GIVEN the root group');
group.test('with one passing test', () => {
  assert(true);
});

const group1 = newGroup('GIVEN a child group', group);
group1.test('a test of the child group', () => {
  assert(true);
});

const group1a = newGroup('GIVEN a 2nd child group', group);
group1a.test('test 1 in child group A', () => {
  assert(true);
});
group1a.test('test 2 in child group A', () => {
  assert(true);
});

const group2 = newGroup('GIVEN a child-child group', group1a);
group2.test('test 1 in child-child group A', () => {
  assert(true);
});
group2.test('test 2 in child-child group A', () => {
  assert(true);
});
