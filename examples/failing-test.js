import {newGroup} from '../src/index.js';
import {strict as assert} from 'assert';

const group = newGroup('GIVEN a test group');
group.test('and a failing test', () => {
  assert(false);
});
