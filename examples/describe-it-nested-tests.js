import {newGroup} from '../src/index.js';
import {strict as assert} from 'assert';

let group;
const it = (name, fn) => {
  group.test(name, fn);
};
const describe = (groupName, fn) => {
  group = newGroup(groupName);
  fn();
};

describe('GIVEN a describe+it test group', () => {
  it('with one passing test', () => {
    assert(true);
  });
  // describe('AND another "describe" inside', () => {
  //   it('AND one it', () => {
  //     assert(true);
  //   });
  // });
});


describe('GIVEN another describe sibling', () => {
  it('with yet another passing test', () => {
    assert(true);
  });
});
