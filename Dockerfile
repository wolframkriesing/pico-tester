FROM node:16
WORKDIR /app
RUN wget https://github.com/SamirTalwar/smoke/releases/download/v2.1.0/smoke-v2.1.0-Linux-x86_64 && \
    mv smoke-v2.1.0-Linux-x86_64 /usr/bin && \
    ln -s /usr/bin/smoke-v2.1.0-Linux-x86_64 /usr/bin/smoke && \
    chmod a+x /usr/bin/smoke

# Set the locale, so smoke will work
# Thanks to https://stackoverflow.com/a/28406007
RUN apt-get update
RUN apt-get -y install locales
RUN sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
    locale-gen
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8
